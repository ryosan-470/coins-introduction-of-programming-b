public class Rectangle3 {
    int width;
    int height;

    Rectangle3() {
	setSize(0, 0);
    }

    Rectangle3(int width, int height) {
	setSize(width, height);
    }

    void setSize(int w, int h) {
	this.width = w;
	this.height = h;
    }

    @Override
    public String toString() {
	return "[ width = " + width + ", height = " + height + " ]";
    }

    public static void main(String[] args) {
	Rectangle3 r = new Rectangle3();
	r.setSize(1, 2);
	System.out.println(r);

	PlacedRectangle r1 = new PlacedRectangle(12,34,56,78);
	System.out.println(r1);
    }
}
class PlacedRectangle extends Rectangle3{
    int x;
    int y;
    PlacedRectangle(){
	setLocation(0, 0);
	setSize(0, 0);
    }
    PlacedRectangle(int x, int y){
	setLocation(x, y);
	setSize(0, 0);
    }
    PlacedRectangle(int x, int y, int width, int height){
	setLocation(x, y);
	setSize(width, height);
    }    
    void setLocation(int x, int y){
	this.x = x;
	this.y = y;
    }

    @Override
    public String toString() {
	return "[("+x+","+y+") ["+width+","+height+"]]";
    }
}

public class Point {
    private int x;
    private int y;

    Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    void printPosition() {
        System.out.println("x: " + x);
        System.out.println("y: " + y);
    }
    public static void main(String[] args) {
        Point p = new Point(1,2);
	p.printPosition();
    }
}

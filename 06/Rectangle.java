public class Rectangle {
    int width;
    int height;

    Rectangle() {
	setSize(10, 20);
    }

    void setSize(int w, int h) {
	width = w;
	height = h;
    }

    int getArea() {
	return width * height;
    }
    public boolean isSame(Rectangle a){
	if(a == null)
	    return false;
	else if(this.width == a.width && this.height == a.height)
	    return true;
	else
	    return false;
	}
    public static void main(String[] args) {
	Rectangle r1 = new Rectangle();
	Rectangle r2 = new Rectangle();
	System.out.println(r1.isSame(r2));
	System.out.println(r2.isSame(r1));
    }
}

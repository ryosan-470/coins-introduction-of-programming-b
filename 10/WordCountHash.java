import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
class WordCountHash {

    static final int SzBkt = 5;

    static int String2Integer(String s) {
	int result = 0;
    
	result = (int)s.charAt(0);

	return result;
    }

    static int HashFunction(int l) {
	return l % SzBkt;
    }

    public static void main (String[] args) {
	ListString [] HashTable = new ListString[SzBkt];
	long start = System.currentTimeMillis();
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	try{
	    String line;
	    while((line = br.readLine()) != null){
		String b[] = line.split(" |\t|\n|,|\\.|;", 0);
		for (int i = 0; i < b.length; i++) {		    
		    if(b[i].equals("") == false){
			int pos = HashFunction(String2Integer(b[i]));
			if(isContains(HashTable, b[i]) == false)
			    HashTable[pos] = ListString.Insert(b[i], HashTable[pos]);
		    }
		}
	    }
	}catch(IOException e){
	    System.out.println(e);
	}
	dump(HashTable, SzBkt);
	long stop = System.currentTimeMillis();
	System.out.println("Exec time :"+(stop-start)+" ms");
    }
    public static void dump(ListString[] table, int size){
	for (int i = 0; i < size; i++) {
	    ListString.Display(table[i]);
	}
    }
    // 線形リスト内に要素keyが含まれているかどうか
    public static boolean isContains(ListString[] table, String key){
	int pos = HashFunction(String2Integer(key));
	if(ListString.Search(table[pos], key))
	    return true;	
	return false;
    }
}

class ListString
{
    String name;
    ListString next;
    int count;
    ListString(String name, ListString tail) {
	this.name=new String(name); 
	this.next=tail;
	count = 1;
    }

    static ListString Insert(String s, ListString list) {
	return new ListString(s, list);
    }

    static void Display(ListString list) {
	while(list!=null) {
	    System.out.println(list.count+" [===] "+list.name);
	    list=list.next;
	}
    }
    static boolean Search(ListString list, String key){
	while(list != null){
	    if(key.equals(list.name)){
		list.count++;
		return true;
	    }
	    list = list.next;
	}
	return false;
    }
}

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
public class WordCountList {
    public static void main(String[] args) {
	long start = System.currentTimeMillis();
	ListString l = new ListString("", null);
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	try {
	    String line;
	    String b[];
	    while((line = br.readLine()) != null){
		b = line.split(" |\t|\n|,|\\.|;", 0);
		for (int i = 0; i < b.length; i++) {
		    l = ListString.Insert(b[i], l);			    
		}
	    }
	} catch (IOException e) {
	}
	ListString.Display(l);
	long stop = System.currentTimeMillis();
	System.out.println("Exec time :"+(stop-start)+" ms");
    }
}

class ListString
{
    int count;
    String name;
    ListString next;

    ListString(String name, ListString tail) {
	this.name = new String(name); 
	this.next = tail;
	count = 1;
    }

    static ListString Insert(String s, ListString list) {
	if(!Search(s, list))
	    return new ListString(s ,list);
	return list;
    }

    static void Display(ListString list) {
	while(list!=null) {
	    if(list.next == null)
		break;
	    System.out.println(list.count+" [===] "+list.name);
	    list = list.next;
	}
    }
    
    static boolean Search(String key, ListString list) {
	while(list != null){
	    if(key.equals(list.name)){
		list.count++;
		return true;
	    }	
	    list = list.next;
	}
	return false;
    }
}

import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
class FindSubSeq {
    public static void main(String args[]){
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	//int tens[] = {0,3,2,3,4,3,2,7,8,9};
	int tens[] = new int[10];
	int[] search = new int[args.length];
	// コマンドライン引数をint型配列に格納
	for(int i=0; i<args.length; i++)
	    search[i] = Integer.parseInt(args[i]);
	// 10個の数字の入力
	System.out.print("Input 10 data: ");
	for(int i=0; i<10; i++){
	try{
	    String line = reader.readLine();
	    tens[i] = Integer.parseInt(line);
	}catch(IOException e){
	    e.printStackTrace();
	}
	}
	
	findSubSeq(tens, search);
    }
    public static void findSubSeq(int[] x, int[] input){
	for(int i=0; i<x.length; i++){
	    int count = 0;
	    int tmp = i;	
	    for(int j=0; j<input.length; j++){
		
	// Debug
	// System.out.println("x["+i+"]:"+x[i]+" input["+j+"]:"+input[j]);
		if(x[tmp] == input[j]){
		    count++;
		    tmp++;
		}
		else
		    break;
	    }
	    if(count == input.length){
		System.out.print("Found at "+(tmp -count) +" Sequence: ");
		for(int k=0; k<input.length; k++)
		    System.out.print(input[k]+" ");
		System.out.println();
	    }
	}
    }
}

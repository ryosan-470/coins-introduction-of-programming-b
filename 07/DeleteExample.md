a b c d e f

Delete d

v == c; w == d
d != null;
v.next = d
w.next = e

v.next = w.next


Delete a
v == a; w == b
v = v.next;


参照渡しと値渡し

参照渡し:変数そのものを弄る
a[0] = 8;

値渡し:関数に渡すときに別の入れ物を作ってそれを弄る
int a[] = {10};
other = a;

関数終了後，コピーした奴は消失するので結果を出力できない

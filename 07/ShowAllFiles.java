import java.io.*;
public class ShowAllFiles {
    public static void main(String[] args) {
	if(args.length != 1){
	    System.out.println("How to Use: java ShowFile directory name");
	    System.out.println("Example: java ShowFile tmp");
	    System.exit(0);
	}
	String dirname = args[0];
	File dir = new File(dirname);	    
	String[] dirlist = dir.list();
	for (int i = 0; i < dirlist.length; i++) {
	    showFile(dirname, dirlist[i]);
	}
    }
    public static void showFile (String dirname, String filename){	
	String file = dirname+"/"+filename;
	System.out.println("File: "+file);
	try{
	    BufferedReader br = new BufferedReader(new FileReader(file));
	    String line;
	    while((line = br.readLine()) != null){
		System.out.println(line);
	    }
	    System.out.println();
	    br.close();
	} catch(FileNotFoundException e){
	    System.out.println(filename+" is not found");
	} catch(IOException e){
	    System.out.println(e);
	}
    }
}

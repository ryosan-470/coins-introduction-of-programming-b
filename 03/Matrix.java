public class Matrix {
    public static void main(String args[]){
	double[][] A =	{ {1.0, 2.0}, {3.0, 4.0} };
	double[][] B =  { {1.5, 0.0}, {0.0, 1.5} };
	double[][] C = new double[2][2];

	for(int i=0; i<A.length; i++){
	    for(int j=0; j<B.length; j++){
		for(int k=0; k<B.length; k++){
		    C[i][j] += A[i][k]*B[k][j];
		}
	    }
	}
	for(int i=0; i<C[0].length; i++){
	    for(int j=0; j<C.length; j++){
		System.out.print(C[i][j]+" ");
	    }
	    System.out.println();
	} 
    }
}

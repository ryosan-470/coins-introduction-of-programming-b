import java.io.*;

public class MaxMin {
    public static void maxMin(int myArray[]){
	int max = myArray[0];
	int min = myArray[0];
	int maxnum = 0;
	int minnum = 0;
	int i;
	for(i=0; i<myArray.length; i++){
	    if(myArray[i] > max){
		max = myArray[i];
		maxnum = i;
	    }
	    if(myArray[i] < min){
		min = myArray[i];
		minnum = i;
	    }
	}
	System.out.println("Max: myArray["+maxnum+"] "+max);
	System.out.println("Min: myArray["+minnum+"] "+min);
    }

    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] myArray;
        try {
            myArray = new int[10];
            System.out.println("Input 10 values");
            for (int i = 0; i < myArray.length; i++) {
                String line = br.readLine();
                myArray[i] = Integer.parseInt(line);
            }
            maxMin(myArray);
        }
        catch (IOException e){
        }
    }
}

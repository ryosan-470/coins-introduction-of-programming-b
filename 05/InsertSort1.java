import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
class InsertSort1 {
    public static void main(String args[]) {
	//	int[] myArray = {3,7,5,4,1};

	int[] myArray;
	int nData = 10;
	if (args.length > 0) {
	    nData = Integer.parseInt(args[0]);
	}
	myArray = new int[nData];
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	String line;

	try {
	    System.out.println("Input " + myArray.length + " data:");
	    for (int i = 0; i < myArray.length; i++) {
		line = reader.readLine();
		myArray[i] = Integer.parseInt(line);
	    }
	
	    insertSort(myArray);
	    System.out.println("Result:");
	    for (int x : myArray) {
		System.out.println(x);
	    }
	}catch(IOException e){
	}
    }

    public static int findPos(int[] a,int p,int key) {
	// 2分探索
	int left = 0; // 探索範囲先頭インデックス
	int right = p - 1; // 探索範囲末尾のインデックス
	do{
	    int center = (left + right) /2;
	    if(a[center] == key)
		return center;
	    else if(a[center] < key)
		left = center + 1;
	    else
		right = center -1;
	}while(left <= right);
	    return left;
    }

    public static void shiftPos(int[] a, int p, int j) {
	while (j >= p) {
	    a[j + 1] = a[j]; 
	    j--;
	}
    }

    public static void insertSort(int[] a) {
	for (int i = 1; i < a.length; i++) {
	    int d = a[i];
	    int p = findPos(a, i, d);
	    shiftPos(a, p, i - 1);
	    a[p] = d;
	}
    }
}

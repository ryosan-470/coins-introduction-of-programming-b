import java.util.Scanner;
class CompareTo{
    public static void main(String args[]){
	Scanner sc = new Scanner(System.in);

	String first = sc.next();
	String second = sc.next();

	if(first.compareTo(second) > 0)
	    System.out.println("1:"+second+","+first);
	else if(first.compareTo(second) < 0)
	    System.out.println("2:"+first+","+second);
	else
	    System.out.println(first +" == "+second);
}
}

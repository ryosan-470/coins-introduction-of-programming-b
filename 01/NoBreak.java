import java.io.*;

class NoBreak {
    public static void main(String args[]){
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	try {
	    String line = reader.readLine();
	    boolean found = true;
	    for(int i=0; i < line.length(); i++){
		if(line.charAt(i) == 'X'){
		    System.out.println("Xが見つかりました.");
		    System.exit(0);
		}
		System.out.print(".");
	    }
	    System.out.println("Xは見つかりませんでした.");
	}catch(IOException e){
	    System.out.println(e);
	}catch(Exception e){
	    System.out.println(e);
	}
    }
}
    

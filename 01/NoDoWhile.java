import java.io.*;

class NoDoWhile {
    public static void main(String args[]){
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	try {
	    String line = reader.readLine();
	    while(!line.equals("bye")){
		System.out.println("Your said: "+line);
		line = reader.readLine();
	    }
	    System.out.println("Your said: "+line);
	    
	}catch(IOException e){
	    System.out.println(e);
	}catch(Exception e){
	    System.out.println(e);
	}
    }
}

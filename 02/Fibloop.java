import java.io.InputStreamReader;
import java.io.BufferedReader;
/*
 * プログラム入門B 設問3
 * フィボナッチ数を求めるプログラム(再帰関数を利用しない)
 */
class Fibloop {
    public static void main(String args[]){
	//Initialized
	int f[] = new int[100000];
	f[0] = 0;
	f[1] = 1;
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	long start = System.currentTimeMillis();
	try{
	    String line = reader.readLine();
	    int x = Integer.parseInt(line);

	    for(int i=2; i <= x; i++){
		f[i] = f[i-1]+f[i-2];
	    }
	    long stop = System.currentTimeMillis();
	    System.out.println(x+"番目のフィボナッチ数は"+f[x]+"です");
	    System.out.println("処理時間は"+(stop-start)+"ミリ秒");
	}catch(Exception e){
	    System.out.println(e);
	}
    }
}

/*
 * プログラム入門B 設問2
 * 放物線を描く メソッドsqとprintGraphはそのまま使う
 * 全部で17行でi行目には星を(i-8)^2 個描かなければならない
 * 結果は実行例を適当なテキストファイルに保存し
 * $ java Parabola > result
 * で作成したものとdiffればいい
 */
class Parabola {
    public static void main(String args[]){
	for(int i=0; i<17; i++){
	    int q = sq(i-8);
	    printGraph(q);
	}
    }
    public static int sq(int x){
	return x*x;
    }
    public static void printGraph(int x){
	for(int i=0; i<x; i++)
	    System.out.print("*");
	System.out.println("");
    }
}
import java.util.Scanner;
/*
 * プログラム入門B 設問4
 * 10進数を2進数にする
 */
class Binary  {
    public static void main(String args[]){
	//Initialized
	int i;
	Scanner sc = new Scanner(System.in);
	i = sc.nextInt();
	printBinary(i);
	System.out.println();

    }
    public static void printBinary(int n){
	if(n > 0){
	    printBinary(n/2);
	    System.out.print(n % 2);
	}	
    }
}

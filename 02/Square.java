/*
 * プログラム入門B 設問1
 * メソッドを利用して1,3,5,7,9,11についての2乗を求める.
 */
class Square {
    public static void main(String args[]){
	//Initialized
	int[] num = {1,3,5,7,9,11}; 
	int ans;
	for(int i=0; i < num.length; i++){
	    ans = sq(num[i]);
	    System.out.println(num[i]+"の2乗は"+ans+"です");
	}
    }
    public static int sq(int x){
	return x*x;
    }
}
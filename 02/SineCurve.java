/*
 * プログラム入門B 設問2
 * 放物線を描く メソッドsqとprintGraphはそのまま使う
 * 全部で21行でi行目には星を30sin(pi*i/10)+30  個描かなければならない
 * 結果は実行例を適当なテキストファイルに保存し
 * $ java SineCurve > result
 * で作成したものとdiffればいい
 */
class SineCurve {
    public static void main(String args[]){
	for(int i=0; i<21; i++){
	    int n = (int(30 * Math.sin(Math.PI * i/10)+30+0.5));
	    int q = sq(i-8);
	    printGraph(q);
	}
    }
    public static int sq(int x){
	return x*x;
    }
    public static void printGraph(int x){
	for(int i=0; i<x; i++)
	    System.out.print("*");
	System.out.println("");
    }
}

import java.util.Scanner;
/*
 * プログラム入門B 設問5
 * アッカーマン関数の値を求めるプログラム
 */

class Ack {
    public static void main(String args[]){
	int m = 0;
	int n = 0;
	// get value
	Scanner sc = new Scanner(System.in);
	m = sc.nextInt();
        n = sc.nextInt();
	System.out.println("ack("+m+","+n+") = "+ack(m, n));
    }
    public static int ack(int m, int n){
	//ack(0, n) = n+1
	if(m == 0)
	    return n+1;
	//ack(m, 0)=ack(m-1, 1) (m>0)
	else if(n == 0 && m > 0)
	    return ack(m-1, 1);
	//ack(m, n) = ack(m-1, ack(m, n-1)) (m>0, n>0)
	else
	    return ack(m-1, ack(m, n-1));
    }
}
